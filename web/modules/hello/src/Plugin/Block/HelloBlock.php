<?php

namespace Drupal\hello\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Hello"),
 * )
 */
class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    /*try {
      $date = \Drupal::moduleHandler()->invokeAll('user_login', [$user])[0];
    } catch (\Exception $e) {
      watchdog_exception('cron', $e);
    }*/
    if ($user->isAuthenticated()) {
      $uid = $user->id();
      $userName = $user->getAccountName();
      $date = date('H:i:s', time());

      return [
        //'#markup' => $this->t('Hello, User!'),
        '#theme' => 'hello_block',
        '#data' => ['uid' => $uid, 'name' => $userName, 'date' => $date],
        '#cache' => ['max-age' => 0],
      ];
    }

    return [
      '#cache' => ['max-age' => 0],
    ];
  }

}

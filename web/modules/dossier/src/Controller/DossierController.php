<?php

namespace Drupal\dossier\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Dossier routes.
 */
class DossierController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
